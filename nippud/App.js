import React from 'react'
import {BrowserRouter as Router, Route, Link, NavLink} from 'react-router-dom'
import Home from './components/Home'
import Phonebook from './components/Phonebook'
import Invoices from './components/Invoices'
import PersonSave from './components/PersonSave'
import PersonUpdate from './components/PersonUpdate'


const App = () => {

    return (
        <div>
          <Router>
            <main>
              <nav>
                <ul>
                  <li><NavLink activeClassName="active" exact to='/'>Home</NavLink></li>
                  <li><NavLink activeClassName="active" to='/phonebook'>Phonebook</NavLink></li>
                  <li><NavLink activeClassName="active" to='/invoices'>Invoices</NavLink></li>
                </ul>
              </nav>
              <Route exact path="/" component={Home} />
              <Route path="/phonebook" component={Phonebook} />
              <Route path="/invoices" component={Invoices} />
              <Route path="/personsave" component={PersonSave} ><PersonSave /></Route>
              <Route path="/personupdate/:id" component={PersonUpdate} ><PersonUpdate /></Route>
            </main>
          </Router>         
        </div>
      ) 
}

export default App