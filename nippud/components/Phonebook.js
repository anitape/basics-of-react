import React, {useState} from 'react';
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom'
import { useForm } from 'react-hook-form'
import PersonTable from './PersonTable';

const Phonebook = () => {
    const [button, isPressed] = useState(false)
    const { handleSubmit } = useForm()
    const [searchText, setSearchText] = useState('')

    const searchTextChanged = (searchMe) => {
        setSearchText(searchMe)
       console.log('This is searchMe of searchTextChanged', searchText)
    }
   
    const handleClick = (data, event) => {
        event.preventDefault()
        console.log('handleClick buttoni on', button)
        isPressed(true)
       // console.log('I changed button to true', button)
        event.target.reset()
        //console.log('handleClick I changed button to true', button)
    }

   const stChanged = (event) => {
        console.log('stChanged button', button)
        searchTextChanged(event.target.value)
        console.log('stChanged Input value is ', event.target.value) 
        isPressed(false)
    //console.log('button is falsen', button)
    }

    return (
        <>
        <h1>Phone Book</h1>
        <form onSubmit={handleSubmit(handleClick)}>
        <input type="text" placeholder="Type a name ..." onChange={stChanged} />
        <button type="submit">Search</button>
        </form>
        <br/>
        <button><Link to='/personsave'>Add New Contact</Link></button>
        <PersonTable searchText={searchText} button={button} />
        </>
    )
}

export default Phonebook