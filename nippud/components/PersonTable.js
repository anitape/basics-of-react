import React, { useState, useEffect } from 'react';
import axios from 'axios'
import PersonRow from './PersonRow'

const PersonTable = (props) => {
    const [owners, setOwners] = useState([])
    const ownerTable = []
    const getOwners = () => {
        axios.get('http://localhost:3001/owners')
        .then(response => {
            console.log('promise fulfilled')
            console.log(response.data)
            setOwners(response.data)
        })
    }

    useEffect(getOwners, [])

    owners.map((owner) => {
        ownerTable.push((!props.button  || props.searchText == '' || owner.name.search(props.searchText) < 0)
         || <PersonRow owner = {owner} key = {owner.id} />
        )    
        console.log('I pushed ownerTable!', ownerTable)
        console.log('Condition ', owner.name.search(props.searchText))
        //console.log('showIt is', props.showIt)        
        console.log('Objectvalue', Object.values(owner.id))
    })

        return(
            <div>                
                <table><tbody>
                    <tr><th>Id</th><th>Name</th><th>Address</th>
                    <th>Postcode</th><th>City</th><th>Phone Number</th></tr>                  
                {ownerTable}                         
                </tbody></table>
                </div> 
        )
}

export default PersonTable