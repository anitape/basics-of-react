import React, { useState } from 'react'
import noteService from './ContactService'
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom'

const PersonRow = (props) => {
    const [notes, setNotes] = useState([])
    const id = props.owner.id
    const name = props.owner.name
    const address = props.owner.address
    const postcode = props.owner.postcode
    const city = props.owner.city
    const phonenum = props.owner.phonenum

    const removeNote = () => {
        noteService
        .remove(id)
          .then(returnedNote => {
          setNotes(notes.map(note => note.id !== id ? note : returnedNote))
        })
      }

    return (
        <tr>
            <td>{id}</td>
            <td>{name}</td>
            <td>{address}</td>
            <td>{postcode}</td>
            <td>{city}</td>
            <td>{phonenum}</td>
            <td><button><Link to={{ pathname: `/personupdate/${id}`, id: id, name: name, address: address, postcode: postcode, city: city, phonenum: phonenum}}>Update</Link></button></td>
            <td><button onClick={removeNote}>Delete</button></td>
            <td><button type="button">Invoice</button></td>
        </tr>
    )
}

export default PersonRow


//<td><button><Link to={`/personupdate/${name}`}>Update</Link></button></td>
// <td><button><Link to={{ pathname: "/personupdate", state: { name: props.owner.name}}}>Update</Link></button></td>
//