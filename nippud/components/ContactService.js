import axios from 'axios'
const baseUrl = 'http://localhost:3001/owners'

const getAll = () => {
  const request = axios.get(baseUrl)
  return request.then(response => response.data)
}

const create = newNote => {
  const request = axios.post(baseUrl, newNote)
  return request.then(response => response.data)
}

const update = (id, newNote) => {
  const request = axios.put(`${baseUrl}/${id}`, newNote)
  return request.then(response => response.data)
}

const remove = (id) => {
  const request = axios.delete(`${baseUrl}/${id}`)
  return request.then(response => response.data)
}

export default { 
  getAll: getAll, 
  create: create, 
  update: update,
  remove: remove 
}