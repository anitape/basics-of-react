import React, { useState, useEffect } from 'react'
import axios from 'axios'
import InvoiceRow from './InvoiceRow'


const InvoiceTable = (props) =>{
    const [invoices, setInvoices] = useState([])
    const invoiceTable = []

    const getInvoices = () => {
        axios.get('http://localhost:3001/invoice')
        .then(response => {
            console.log('promise fulfilled')
            console.log(response.data)
            setInvoices(response.data)
        })
    }

    useEffect(getInvoices, [])

    invoices.map((invoice) => {
        invoiceTable.push((!props.button  || props.searchText == '' || invoice.id != props.searchText)
         || <InvoiceRow invoice = {invoice} key = {invoice.number} />
        )    
        console.log('I pushed invoiceTable!', invoiceTable)
        console.log('Id!', invoice.id)
    })

        return(
            <div>                
                <table><tbody>
                    <tr><th>Id</th><th>Number</th><th>Sum</th>
                    <th>Date</th><th>Details</th><th>Payment</th></tr>                  
                {invoiceTable}                         
                </tbody></table>
                </div> 
        )
}


export default InvoiceTable