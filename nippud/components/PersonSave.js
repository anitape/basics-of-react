import React, {useState} from 'react';
import noteService from './ContactService'

const PersonSave = () => {
    const [notes, setNotes] = useState([])
    const [newNote, setNewNote] = useState({ id: '', name: '', address: '', postcode: '', city: '',  phonenum: ''})

    const addNote = event => {
      event.preventDefault()
      noteService
      .create(newNote)
        .then(returnedNote => {
        setNotes(notes.concat(returnedNote))
        setNewNote('')
      })
      event.target.reset()
    }
  
    const handleNoteChange = event => {
        const { name, value } = event.target
        setNewNote(prevState => ({
            ...prevState,
            [name]: value
        }))
    }

    return(
        <>
        <h1>Create New Contact</h1>
        <form onSubmit={addNote}>
        <input name='id' onInput={handleNoteChange} placeholder='Id' />
        <input name='name' onInput={handleNoteChange} placeholder='Name' />
        <br />
        <input name='address' onInput={handleNoteChange} placeholder='Address' />
        <input name='postcode' onInput={handleNoteChange} placeholder='Postcode' />
        <input name='city' onInput={handleNoteChange} placeholder='City' />
        <br />
        <input name='phonenum' onInput={handleNoteChange} placeholder='Phone number' />
        <br />
        <button type="submit">save</button>
        </form>
        </>
    ) 
}

export default PersonSave