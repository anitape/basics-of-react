import React, {useState} from 'react';
import noteService from './ContactService'
import { useLocation } from "react-router-dom"

const PersonUpdate = () => {
    const [notes, setNotes] = useState([])
    const { id, name, address, postcode, city, phonenum } = useLocation()
    const [changedNote, setChangedNote] = useState({ id: id, name: name, address: address, postcode: postcode, city: city,  phonenum: phonenum})

    const updateNote = event => {
      noteService
      .update(id, changedNote)
        .then(returnedNote => {
        setNotes(notes.map(note => note.id !== id ? note : returnedNote))
      })
      event.target.reset()
    }
  
    const handleNoteChange = event => {
        const { name, value } = event.target
        setChangedNote(prevState => ({
            ...prevState,
            [name]: value
        }))
    }
    
    return(
        <>
        <h1>Update Contact</h1>
        <form onSubmit={updateNote}>
        <input name='id' defaultValue={id} onInput={handleNoteChange} placeholder='Id' />
        <input name='name'  defaultValue={name} onInput={handleNoteChange} placeholder='Name' />
        <br />
        <input name='address' defaultValue={address} onInput={handleNoteChange} placeholder='Address' />
        <input name='postcode' defaultValue={postcode} onInput={handleNoteChange} placeholder='Postcode' />
        <input name='city' defaultValue={city} onInput={handleNoteChange} placeholder='City' />
        <br />
        <input name='phonenum' defaultValue={phonenum} onInput={handleNoteChange} placeholder='Phone number' />
        <br />
        <button type='submit'>Update</button>
        </form>
        </>
    ) 
}

export default PersonUpdate