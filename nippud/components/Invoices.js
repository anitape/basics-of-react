import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import InvoiceTable from './InvoiceTable'

const Invoices = () => {
    const [button, isPressed] = useState(false)
    const { handleSubmit } = useForm()
    const [searchText, setSearchText] = useState('')

    const searchTextChanged = (searchMe) => {
        setSearchText(searchMe)
       console.log('This is searchMe of searchTextChanged', searchText)
    }
   
    const handleClick = (data, event) => {
        event.preventDefault()
        console.log('handleClick buttoni on', button)
        isPressed(true)
       // console.log('I changed button to true', button)
        event.target.reset()
        //console.log('handleClick I changed button to true', button)
    }

   const stChanged = (event) => {
        console.log('stChanged button', button)
        searchTextChanged(event.target.value)
        console.log('stChanged Input value is ', event.target.value) 
        isPressed(false)
    //console.log('button is falsen', button)
    }

    return(
        <div>
            <h1>Invoices</h1>
            <form onSubmit={handleSubmit(handleClick)}>
            <input type="text" placeholder="Type an id ..." onChange={stChanged} />
            <button type="submit">Search</button>
            </form>
            <InvoiceTable searchText={searchText} button={button} />
        </div>
    )
}

export default Invoices