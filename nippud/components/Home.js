import React from 'react';

const Home = () => {
    return(
        <div>
            <h1>My Home Page</h1>
                <h3>Anita Petunova</h3>
                <p>Metropolia ammattikorkeakoulu</p>
                <p>anita.petunova@metropolia.fi</p>
        </div>
    )
}

export default Home