import React, { useState } from 'react'
import billService from './ContactService'
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom'

const InvoiceRow = (props) => {
    const [bills, setBills] = useState([])
    const id = props.invoice.id
    const number = props.invoice.number
    const sum = props.invoice.sum
    const date = props.invoice.date
    const details = props.invoice.details
    const payment = props.invoice.payment

    const removeBill = () => {
        billService
        .remove(id)
          .then(returnedBill => {
          setBills(bills.map(bill => bill.id !== id ? bill : returnedBill))
        })
      }

    return (
        <tr>
            <td>{id}</td>
            <td>{number}</td>
            <td>{sum}</td>
            <td>{date}</td>
            <td>{details}</td>
            <td>{payment}</td>
            <td><button><Link to={{ pathname: `/personupdate/${id}`, id: id, number: number, sum: sum, date: date, details: details, payment: payment}}>Update</Link></button></td>
            <td><button onClick={removeBill}>Delete</button></td>
            <td><button type="button">Invoice</button></td>
        </tr>
    )
}

export default InvoiceRow