import React from 'react';
import CarList from './components/CarList'
import CarTable from './components/CarTable'

const cars = [{id:"1", merkki: "Toyota", malli:"Yaris", vari: "grey"},
{id:"2", merkki: "Volkswagen", malli:"Passat", vari: "blue"},
{id:"3", merkki: "Nissan", malli:"Quashqai", vari: "white"},
{id:"4", merkki: "BMW", malli:"X5", vari: "black"},
{id:"5", merkki: "Audi", malli:"A8", vari: "red"}
]

const App = () => {
    return (
        <div>
        <h1>Autot</h1>
        <CarList cars={cars}></CarList>
        <CarTable cars={cars}/>
        </div>
    );
}

export default App