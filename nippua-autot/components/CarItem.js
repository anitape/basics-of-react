import React from 'react';

const CarItem = ({car}) => {
    return (
        <li>{car.merkki}, {car.malli}, {car.vari}</li>
    )

}

export default CarItem