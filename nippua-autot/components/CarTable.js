import React from 'react';
import CarRow from './CarRow'

const CarTable = (props) => {
    const carTable = []
    props.cars.forEach((car) => {
    carTable.push(<CarRow car={car} key={car.merkki}/>)})
    const carEnglish = []
    props.cars.forEach((car) => {
    carEnglish.push(<CarRow car={car} key={car.id}/>)})
    return(
        <>
        <table><tbody>
            <tr><th>Merkki</th><th>Malli</th><th>Väri</th></tr>           
            {carTable}
        </tbody></table>
        <br/>
        <table><tbody>
        <tr><th>Brand</th><th>Model</th><th>Color</th></tr>           
        {carEnglish}
    </tbody></table>
    </>
    )
}

export default CarTable