import React from 'react';

const CarRow = (props) => {
    return(
        <tr>
        <td>{props.car.merkki}</td>
        <td>{props.car.malli}</td>
        <td>{props.car.vari}</td>
      </tr>
    )
}

export default CarRow