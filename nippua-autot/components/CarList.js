import React from 'react';
import CarItem from './CarItem'

const CarList = (props) => {
    return (
        <ul>
            {props.cars.map((car) => {
               return <CarItem car={car} key={car.id}/>
            })}
        </ul>

    )
}

export default CarList