import React, { useState, useEffect } from 'react';
import axios from 'axios'
import PersonRow from './PersonRow'

const PersonTable = (props) => {
    const [owners, setOwners] = useState([])
    const getOwners = () => {
        axios.get('http://localhost:3001/owners')
        .then(response => {
            console.log('promise fulfilled')
            console.log(response.data)
            setOwners(response.data)
        })
    }

    useEffect(getOwners, [])

    const ownerTable = []

    const [invoices, setInvoices] = useState([])
    const getInvoices = () => {
        axios.get('http://localhost:3001/invoice')
        .then(response => {
            console.log('GetInvoices promise fulfilled')
            console.log(response.data)
            setInvoices(response.data)
        })
        console.log('Invoices', invoices)

    }

    useEffect(getInvoices, [])
    const invoiceTable = []

    invoices.forEach((invoice) => {
        invoiceTable.push(invoice)
        console.log('invoice pushing', invoice)
        console.log('invoiceTable is', invoiceTable)
    }
    )

    const persondata = owners.map((item, id) => {
        const data = invoiceTable[id]
       
        return (
            <>

            </>
        )
    })
   
    owners.forEach((owner) => {
        ownerTable.push((!props.button  || props.searchText == '' || owner.name.search(props.searchText) < 0 || owner.postcode.search(props.searchPost) < 0)
         || <PersonRow owner = {owner} key = {owner.id} invoiceTable={invoiceTable} />
        )    
        console.log('I pushed ownerTable!', ownerTable)
        console.log('Condition ', owner.name.search(props.searchText))
        //console.log('showIt is', props.showIt)        
        console.log('Objectvalue', Object.values(owner.id))
    })

    

        return(
            <div>                
                <table><tbody>
                    <tr><th>Id</th><th>Name</th><th>Address</th>
                    <th>Postcode</th><th>City</th><th>Phone Number</th></tr>                  
                {ownerTable}                         
                </tbody></table>
                </div> 
        )
}

export default PersonTable