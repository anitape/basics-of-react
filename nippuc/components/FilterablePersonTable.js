import React, { useState } from 'react'
import SearchTable from './SearchTable'
import PersonTable from './PersonTable'

const FilterablePersonTable = () => {
    const [showIt, setIt] = useState(false)
    const [button, isPressed] = useState(false)


    const [searchText, setSearchText] = useState('')
    const searchTextChanged = (searchMe) => {
        setSearchText(searchMe)
       console.log('This is searchMe of searchTextChanged', searchText)
    }

    const [searchPost, setSearchPost] = useState('')
    const searchPostChanged = (searchPost) => {
        setSearchPost(searchPost)
       console.log('This is searchMe of searchTextChanged', searchPost)
    }
 
    return (
        <>
        <SearchTable searchTextChanged={searchTextChanged} searchPostChanged={searchPostChanged} button={button} isPressed={isPressed} />
        <PersonTable searchText={searchText} searchPost={searchPost} button={button} />
        </>
    )

}

export default FilterablePersonTable