import React, { useState, useEffect } from 'react';
import axios from 'axios'

const PersonRow = (props) => {

    return (
        <tr>
            <td>{props.owner.id}</td>
            <td>{props.owner.name}</td>
            <td>{props.owner.address}</td>
            <td>{props.owner.postcode}</td>
            <td>{props.owner.city}</td>
            <td>{props.owner.phonenum}</td>
            <td style={{display: props.invoiceTable.payment == false ? 'hidden' : 'visible'}}><button type="button">Invoice</button></td>
        </tr>
    )
}

export default PersonRow