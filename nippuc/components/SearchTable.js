import React, { useState } from 'react'
import { useForm } from 'react-hook-form'

const SearchTable = ({searchTextChanged, searchPostChanged, button, isPressed}) => {
    const { handleSubmit } = useForm()

    
    const handleClick = (data, event) => {
        event.preventDefault()
        console.log('handleClick buttoni on', button)
        isPressed(true)
       // console.log('I changed button to true', button)
        event.target.reset()
        //console.log('handleClick I changed button to true', button)
    }

   const stChanged = (event) => {
       console.log('stChanged button', button)
        searchTextChanged(event.target.value)
        console.log('stChanged Input value is ', event.target.value) 
        isPressed(false)
    //console.log('button is falsen', button)
    }

    const ptChanged = (event) => {
        console.log('stChanged button', button)
         searchPostChanged(event.target.value)
         console.log('stChanged Input value is ', event.target.value) 
         isPressed(false)
     //console.log('button is falsen', button)
     }

    return (
        <form onSubmit={handleSubmit(handleClick)}>
        <input type="text" placeholder="Type a name ..." onChange={stChanged} />
        <input type="text" placeholder="Type a postcode ..." onChange={ptChanged} />
        <button type="submit">Search</button>
        </form>
    )
}

export default SearchTable