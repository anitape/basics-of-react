import React from 'react'
import FilterablePersonTable from './components/FilterablePersonTable'

const App = () =>{
    return (
        <FilterablePersonTable/>
    ) 
}

export default App