import React, { useState, useEffect, useContext } from 'react';
import styles from './css/style.css';
import OrderTable from './components/OrderTable';
import FilterableProductTable from './components/FilterableProductTable';
import {BrowserRouter as Router, Route, Link, NavLink} from 'react-router-dom'
import Home from './components/Home'

const App = () => {
  return (
    <div>
      <Router>
        <main>
          <nav>
            <ul>
              <li><NavLink activeClassName="active" exact to='/'>Home</NavLink></li>
              <li><NavLink activeClassName="active" to={'/filterableproducttable'}>Products</NavLink></li>
              <li><NavLink activeClassName="active" to='/ordertable'>Orders</NavLink></li>
            </ul>
          </nav>
          <Route exact path="/" component={Home} />
          <Route path="/filterableproducttable" component={FilterableProductTable} />
          <Route path="/ordertable" component={OrderTable} />
        </main>
      </Router>          
    </div>
  );
}

export default App;