import React from 'react'

const Message = (props) => {
    return(
        <div className={'message-'+props.tyyli}>
            {props.children}
            <hr/>
            <p>Minun message</p>
        </div>
    )
}

export default Message