import React, {useState} from 'react'
import '../App.css'

const Message = (props) => {
    const [message, setShowResult] = useState(false)

    const hideText = () => {
        setShowResult(true)
    }

    return(
    <div className={!message ? 'visible': 'hidden'}>
    <h1>My first React App</h1>
    <footer>
        <div>
        <p>This is footer</p>
        <p>Number of people {props.list.length}</p>
        </div>
        <button type="button" onClick={hideText}>OK</button>
    </footer>
    </div>
    )
}

export default Message