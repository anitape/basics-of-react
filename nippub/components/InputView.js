import React, {useState} from 'react'
import { useForm } from 'react-hook-form'
import ContactTable from './ContactTable'
import '../App.css'
import Message from './Message'

const InputView = (props) => {
    const [state, setState] = useState({ id: '', firstName: '', address: '', city: ''})
    const[list, setList] = useState([])
    const { register, handleSubmit } = useForm()
    const [count, userCount] = useState(1)


    const onSubmit = (data, event) => {
        event.preventDefault();
        userCount(count+1)
        console.log(count)
        state.id = count;
        list.push(state)
        console.log('I pushed the state', state)
        event.target.reset(); // reset after form submit 
        setState({ id: '', firstName: '', address: '', city: ''})   
    }
    
    const setName = event => {
        const { name, value } = event.target
        setState(prevState => ({
            ...prevState,
            [name]: value
        }))
    }

    const handleUppercase = event =>{
        const { name, value } = event.target
        setState(prevState => ({
            ...prevState, 
            [name]: value.toUpperCase()
        }))
    }
    
    return(
        <>
        <div className={list.length > 5 ? 'visible': 'hidden'}><Message list={list}/></div>
        <form onSubmit={handleSubmit(onSubmit)}>
        <input type='text' name='firstName' placeholder='Name' onInput={setName} onInput={handleUppercase} ref={register({ required: true })}/>
        <input type='text' name='address' placeholder='Address' onInput={setName}/>
        <select name='city' onInput={setName}>
            <option value="" hidden="hidden">Select City</option>
            <option value="Espoo">Espoo</option>
            <option value="Helsinki">Helsinki</option>
            <option value="Tampere">Tampere</option>
            <option value="Turku">Turku</option>
            <option value="Vantaa">Vantaa</option>
        </select>
        <br/>
        <button type="submit">Submit</button>
        <br/>
         <ContactTable list={list} setList={setList}/>   
        </form>
        </>
    )
}

export default InputView