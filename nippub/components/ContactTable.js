import React from 'react'
import ContactRow from './ContactRow'

const ContactTable = (props) => {
    const contactView = []

    props.list.map((contact) => {
        contactView.push(<ContactRow list={props.list} setList={props.setList} contact={contact} key={contact.id} />)
    })
    return(
    <table style={{border: props.list.length > 5 ? '1px solid red' : '1px solid black'}}><tbody>
        <tr><th>Id</th><th>Name</th><th>Address</th>
        <th>City</th>
        <th>Operation</th></tr>
        {contactView}
    </tbody>
        </table>
)}

export default ContactTable