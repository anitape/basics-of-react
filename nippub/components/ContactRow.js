import React from 'react'

const ContactRow = (props) => {

    const handleRemove = id => {
        const newList = props.list.filter(item => item.id !== id)
        //console.log('New list is ', newList)
        props.setList(newList)
    }

    return(
        <tr><td>{props.contact.id}</td>
        <td>{props.contact.firstName}</td>
        <td>{props.contact.address}</td>
        <td>{props.contact.city}</td>
        <td><button type="button" onClick={() => handleRemove(props.contact.id)}>Delete</button></td></tr>
    )
}

export default ContactRow