import React, {useState} from 'react';


const Laskuri = () =>  {
    const [laskuri, setCount] = useState(0);  
   return(
    <>   
    <p>Arvo on <span className={laskuri >= 0 ? '': 'negative'} >{laskuri} </span></p>
    <button onClick={() => setCount(laskuri +1)} >Lisää</button>
    <button onClick={() => setCount(laskuri -1)} >Vähennä</button>
    </>
    )}

export default Laskuri